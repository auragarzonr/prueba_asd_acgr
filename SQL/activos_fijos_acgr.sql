-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.40-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for activos_fijos_acgr
DROP DATABASE IF EXISTS `activos_fijos_acgr`;
CREATE DATABASE IF NOT EXISTS `activos_fijos_acgr` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `activos_fijos_acgr`;

-- Dumping structure for table activos_fijos_acgr.activos_fijos
DROP TABLE IF EXISTS `activos_fijos`;
CREATE TABLE IF NOT EXISTS `activos_fijos` (
  `id_activo_fijo` int(2) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `tipo` varchar(200) DEFAULT NULL,
  `serial` varchar(200) DEFAULT NULL,
  `numeroInternoInventario` varchar(200) DEFAULT NULL,
  `peso` varchar(200) DEFAULT NULL,
  `ancho` varchar(200) DEFAULT NULL,
  `alto` varchar(200) DEFAULT NULL,
  `largo` varchar(200) DEFAULT NULL,
  `valorCompra` double DEFAULT NULL,
  `fechaCompra` date DEFAULT NULL,
  `fechaDeBaja` date DEFAULT NULL,
  `estadoActual` int(2) DEFAULT NULL,
  `color` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_activo_fijo`),
  KEY `estado_actual_fk_idx` (`estadoActual`),
  CONSTRAINT `estado_actual_fk` FOREIGN KEY (`estadoActual`) REFERENCES `estado_actual` (`id_estado_actual`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table activos_fijos_acgr.activos_fijos: ~12 rows (approximately)
/*!40000 ALTER TABLE `activos_fijos` DISABLE KEYS */;
INSERT INTO `activos_fijos` (`id_activo_fijo`, `nombre`, `descripcion`, `tipo`, `serial`, `numeroInternoInventario`, `peso`, `ancho`, `alto`, `largo`, `valorCompra`, `fechaCompra`, `fechaDeBaja`, `estadoActual`, `color`) VALUES
	(1, 'BARACK OBAMA', 'descripcion1', 'tipo 1', 'serial', 'NoInternoInventario 1', 'perso 1', 'ancho 1', 'alto 1', 'largo 1', 100, '2019-03-05', '2019-03-05', 1, 'color 1'),
	(2, 'Nombre 1 ', 'DESCRIPCION', 'Tipo 2', 'SERIAL', 'NoInternoInventario 1', 'PESO 1', 'ANCHO 1', 'ALTO 1', 'LARGO 1', 100, '2019-03-05', '2019-03-05', 1, 'AZUL'),
	(3, 'Nombre 1 ', 'DESCRIPCION', 'Tipo 3', 'SERIAL', 'NoInternoInventario 1', 'PESO 1', 'ANCHO 1', 'ALTO 1', 'LARGO 1', 100, '2019-03-05', '2019-03-05', 1, 'AZUL'),
	(4, 'Nombre 1 ', 'DESCRIPCION', 'Tipo 4', 'SERIAL', 'NoInternoInventario 1', 'PESO 1', 'ANCHO 1', 'ALTO 1', 'LARGO 1', 100, '2019-03-05', '2019-03-05', 1, 'AZUL'),
	(5, 'Nombre 1 ', 'DESCRIPCION', 'Tipo 5', 'SERIAL', 'NoInternoInventario 1', 'PESO 1', 'ANCHO 1', 'ALTO 1', 'LARGO 1', 100, '2019-03-05', '2019-03-05', 1, 'AZUL'),
	(6, 'nombre1', 'descripcion1', 'Tipo 6', 'serial', 'NoInternoInventario 1', 'perso 1', 'ancho 1', 'alto 1', 'largo 1', 100, '2019-03-05', '2019-03-05', 1, 'color 1'),
	(7, 'nombre1', 'descripcion1', 'Tipo 7', 'serial', 'NoInternoInventario 1', 'perso 1', 'ancho 1', 'alto 1', 'largo 1', 100, '2019-03-05', '2019-03-05', 1, 'color 1'),
	(8, 'nombre1', 'descripcion1', 'Tipo 8', 'serial', 'NoInternoInventario 1', 'perso 1', 'ancho 1', 'alto 1', 'largo 1', 100, '2019-03-05', '2019-03-05', 1, 'color 1'),
	(9, 'nombre1', 'descripcion1', 'Tipo 9', 'serial', 'NoInternoInventario 1', 'perso 1', 'ancho 1', 'alto 1', 'largo 1', 100, '2019-03-05', '2019-03-05', 1, 'color 1'),
	(10, 'nombre1', 'descripcion1', 'tipo 1', 'serial', 'NoInternoInventario 1', 'perso 1', 'ancho 1', 'alto 1', 'largo 1', 100, '2019-03-05', '2019-03-05', 1, 'color 1'),
	(11, 'nombre1', 'descripcion1', 'tipo 1', 'serial', 'NoInternoInventario 1', 'perso 1', 'ancho 1', 'alto 1', 'largo 1', 100, '2019-03-05', '2019-03-05', 1, 'color 1'),
	(12, 'nombre1', 'descripcion1', 'tipo 1', 'serial', 'NoInternoInventario 1', 'perso 1', 'ancho 1', 'alto 1', 'largo 1', 100, '2019-03-05', '2019-03-05', 1, 'color 1');
/*!40000 ALTER TABLE `activos_fijos` ENABLE KEYS */;

-- Dumping structure for table activos_fijos_acgr.areas
DROP TABLE IF EXISTS `areas`;
CREATE TABLE IF NOT EXISTS `areas` (
  `id_area` int(2) NOT NULL,
  `area` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table activos_fijos_acgr.areas: ~4 rows (approximately)
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;
INSERT INTO `areas` (`id_area`, `area`) VALUES
	(1, 'AREA 1'),
	(2, 'AREA 2'),
	(3, 'AREA 3'),
	(4, 'AREA 4');
/*!40000 ALTER TABLE `areas` ENABLE KEYS */;

-- Dumping structure for table activos_fijos_acgr.areas_ciudades
DROP TABLE IF EXISTS `areas_ciudades`;
CREATE TABLE IF NOT EXISTS `areas_ciudades` (
  `id_area_ciudad` int(2) NOT NULL AUTO_INCREMENT,
  `id_area` int(2) NOT NULL,
  `id_ciudad` int(2) NOT NULL,
  PRIMARY KEY (`id_area_ciudad`,`id_area`,`id_ciudad`),
  KEY `areas_fk_idx` (`id_area`),
  KEY `ciudades_fk_idx` (`id_ciudad`),
  CONSTRAINT `areas_fk` FOREIGN KEY (`id_area`) REFERENCES `areas` (`id_area`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ciudades_fk` FOREIGN KEY (`id_ciudad`) REFERENCES `ciudades` (`id_ciudad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table activos_fijos_acgr.areas_ciudades: ~0 rows (approximately)
/*!40000 ALTER TABLE `areas_ciudades` DISABLE KEYS */;
/*!40000 ALTER TABLE `areas_ciudades` ENABLE KEYS */;

-- Dumping structure for table activos_fijos_acgr.asignaciones_activos_fijos_personas
DROP TABLE IF EXISTS `asignaciones_activos_fijos_personas`;
CREATE TABLE IF NOT EXISTS `asignaciones_activos_fijos_personas` (
  `id_activo_fijo` int(2) NOT NULL,
  `id_persona` int(2) NOT NULL,
  `fecha_asignacion` date DEFAULT NULL,
  PRIMARY KEY (`id_activo_fijo`,`id_persona`),
  KEY `id_persona_fk_idx` (`id_persona`),
  CONSTRAINT `id_activo_fijo_fk` FOREIGN KEY (`id_activo_fijo`) REFERENCES `activos_fijos` (`id_activo_fijo`),
  CONSTRAINT `id_persona_fk` FOREIGN KEY (`id_persona`) REFERENCES `personas` (`id_persona`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table activos_fijos_acgr.asignaciones_activos_fijos_personas: ~0 rows (approximately)
/*!40000 ALTER TABLE `asignaciones_activos_fijos_personas` DISABLE KEYS */;
/*!40000 ALTER TABLE `asignaciones_activos_fijos_personas` ENABLE KEYS */;

-- Dumping structure for table activos_fijos_acgr.ciudades
DROP TABLE IF EXISTS `ciudades`;
CREATE TABLE IF NOT EXISTS `ciudades` (
  `id_ciudad` int(2) NOT NULL,
  `ciudad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_ciudad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table activos_fijos_acgr.ciudades: ~4 rows (approximately)
/*!40000 ALTER TABLE `ciudades` DISABLE KEYS */;
INSERT INTO `ciudades` (`id_ciudad`, `ciudad`) VALUES
	(1, 'CALI'),
	(2, 'BOGOTA'),
	(3, 'BUCARAMANGA'),
	(4, 'PASTO');
/*!40000 ALTER TABLE `ciudades` ENABLE KEYS */;

-- Dumping structure for table activos_fijos_acgr.estado_actual
DROP TABLE IF EXISTS `estado_actual`;
CREATE TABLE IF NOT EXISTS `estado_actual` (
  `id_estado_actual` int(2) NOT NULL,
  `estado_actual` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_estado_actual`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table activos_fijos_acgr.estado_actual: ~4 rows (approximately)
/*!40000 ALTER TABLE `estado_actual` DISABLE KEYS */;
INSERT INTO `estado_actual` (`id_estado_actual`, `estado_actual`) VALUES
	(1, 'ACTIVO'),
	(2, 'DADOBAJA'),
	(3, 'ENREPARACION'),
	(4, 'DISPONIBLE'),
	(5, 'ASIGNADO');
/*!40000 ALTER TABLE `estado_actual` ENABLE KEYS */;

-- Dumping structure for table activos_fijos_acgr.personas
DROP TABLE IF EXISTS `personas`;
CREATE TABLE IF NOT EXISTS `personas` (
  `id_persona` int(2) NOT NULL,
  `nombre_persona` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_persona`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table activos_fijos_acgr.personas: ~4 rows (approximately)
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` (`id_persona`, `nombre_persona`) VALUES
	(1, 'JORGE PINZON'),
	(2, 'HERNAN PRECIADO'),
	(3, 'BARACK OBAMA'),
	(4, 'MARITZA GUTIERREZ');
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
