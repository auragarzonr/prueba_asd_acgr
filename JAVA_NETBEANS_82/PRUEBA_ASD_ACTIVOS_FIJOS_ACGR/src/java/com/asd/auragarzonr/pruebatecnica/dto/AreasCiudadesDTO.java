/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 5 de Marzo de 201911:21 AM
 */
@XmlRootElement
public class AreasCiudadesDTO {
    @XmlElement private int id_area_ciudad;
    @XmlElement private int id_area;
    @XmlElement private int id_ciudad;    

    public int getId_area_ciudad() {
        return id_area_ciudad;
    }

    public void setId_area_ciudad(int id_area_ciudad) {
        this.id_area_ciudad = id_area_ciudad;
    }

    public int getId_area() {
        return id_area;
    }

    public void setId_area(int id_area) {
        this.id_area = id_area;
    }

    public int getId_ciudad() {
        return id_ciudad;
    }

    public void setId_ciudad(int id_ciudad) {
        this.id_ciudad = id_ciudad;
    }
    
    
}
