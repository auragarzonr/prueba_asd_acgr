/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.dto.resultados;

import com.asd.auragarzonr.pruebatecnica.dto.PersonasDTO;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User
 */
@XmlRootElement
public class Resultado_consultarPersonasDTO {
    
    @XmlElement private ResultadoGenericoDTO resultadoGenericoDTO;
    @XmlElement List<PersonasDTO> l_resultado;

    public Resultado_consultarPersonasDTO() {
        resultadoGenericoDTO = new ResultadoGenericoDTO();
    }

    public ResultadoGenericoDTO getResultadoGenericoDTO() {
        return resultadoGenericoDTO;
    }

    public void setResultadoGenericoDTO(ResultadoGenericoDTO resultadoGenericoDTO) {
        this.resultadoGenericoDTO = resultadoGenericoDTO;
    }

    public List<PersonasDTO> getL_resultado() {
        return l_resultado;
    }

    public void setL_resultado(List<PersonasDTO> l_resultado) {
        this.l_resultado = l_resultado;
    }
    
    
    
}
