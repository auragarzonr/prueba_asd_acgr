/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.dao;

import com.asd.auragarzonr.pruebatecnica.dto.ActivosFijosDTO;
import com.asd.auragarzonr.pruebatecnica.dto.AreasDTO;
import com.asd.auragarzonr.pruebatecnica.dto.consulta.ConsultaActivosFijosDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.asd.auragarzonr.pruebatecnica.util.Utilitario;
import org.apache.log4j.Logger;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 5 de marzo de 2019
 */
public class ActivosFijosDAO {
    
    private final Connection conexion;
    final static Logger logger = Logger.getLogger(AreasDAO.class);

    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de Marzo de 2019 11:48 AM
     * @param conexion 
     * 
     */
    
    public ActivosFijosDAO(Connection conexion) {
        this.conexion = conexion;
    }
    
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de Marzo de 2019 11:49 AM
     * @param activosFijosDTO 
     */
    
    public int insert(ActivosFijosDTO activosFijosDTO) throws Exception{
        try{
            String sql = "INSERT INTO activos_fijos_acgr.activos_fijos " +
                            "(" +
                            "nombre," +
                            "descripcion," +
                            "tipo," +
                            "serial," +
                            "numeroInternoInventario," +
                            "peso," +
                            "ancho," +
                            "alto," +
                            "largo," +
                            "valorCompra," +
                            "fechaCompra," +
                            "fechaDeBaja," +
                            "estadoActual," +
                            "color)" +
                            "VALUES" +
                            "(" + // id_activo_fijo 1
                            "?," + // nombre 2 
                            "?," + // descripcion 3
                            "?," +  //tipo 4
                            "?," +  //serial 5
                            "?," +  //numeroInternoInventario 6
                            "?," +  //peso 7
                            "?," +  //ancho 8
                            "?," +  //alto 9
                            "?," +  //largo 10
                            "?," +  //valorCompra 11
                            "?," +  //fechaCompra 12
                            "?," +  //fechaDeBaja 13
                            "?," +  //estadoActual 14
                            "?);";  //color 15
            
            PreparedStatement stmt = this.conexion.prepareStatement(sql);
            stmt.setString(1, activosFijosDTO.getNombre());
            stmt.setString(2, activosFijosDTO.getDescripcion());
            stmt.setString(3, activosFijosDTO.getTipo());
            stmt.setString(4, activosFijosDTO.getSerial());
            stmt.setString(5, activosFijosDTO.getNumeroInternoInventario());
            stmt.setString(6, activosFijosDTO.getPeso());
            stmt.setString(7, activosFijosDTO.getAncho());
            stmt.setString(8, activosFijosDTO.getAlto());
            stmt.setString(9, activosFijosDTO.getLargo());
            stmt.setString(10, activosFijosDTO.getValorCompra());
            stmt.setString(11, activosFijosDTO.getFechaCompra());
            stmt.setString(12, activosFijosDTO.getFechaDeBaja());
            stmt.setString(13, activosFijosDTO.getEstadoActual());
            stmt.setString(14, activosFijosDTO.getColor());
            
            logger.info("Entro al metodo encargado de la generacion de activos fijos");
            
            int resultado=stmt.executeUpdate();
            stmt.close();
            return resultado;
        }catch(Exception e){
            throw e;
        }
    }
    
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de Marzo de 2019 11:49 AM
     * @param activosFijosDTO 
     */
    
    public int update(ActivosFijosDTO activosFijosDTO) throws Exception{
        try{
            String sql = "UPDATE activos_fijos_acgr.activos_fijos SET " +
                            "nombre=?," +
                            "descripcion=?," +
                            "tipo=?," +
                            "serial=?," +
                            "numeroInternoInventario=?," +
                            "peso=?," +
                            "ancho=?," +
                            "alto=?," +
                            "largo=?," +
                            "valorCompra=?," +
                            "fechaCompra=?," +
                            "fechaDeBaja=?," +
                            "estadoActual=?," +
                            "color=? WHERE id_activo_fijo=?";
            
            PreparedStatement stmt = this.conexion.prepareStatement(sql);
            
            stmt.setString(1, activosFijosDTO.getNombre());
            stmt.setString(2, activosFijosDTO.getDescripcion());
            stmt.setString(3, activosFijosDTO.getTipo());
            stmt.setString(4, activosFijosDTO.getSerial());
            stmt.setString(5, activosFijosDTO.getNumeroInternoInventario());
            stmt.setString(6, activosFijosDTO.getPeso());
            stmt.setString(7, activosFijosDTO.getAncho());
            stmt.setString(8, activosFijosDTO.getAlto());
            stmt.setString(9, activosFijosDTO.getLargo());
            stmt.setString(10, activosFijosDTO.getValorCompra());
            stmt.setString(11, activosFijosDTO.getFechaCompra());
            stmt.setString(12, activosFijosDTO.getFechaDeBaja());
            stmt.setString(13, activosFijosDTO.getEstadoActual());
            stmt.setString(14, activosFijosDTO.getColor());
            stmt.setString(15, activosFijosDTO.getId_activo_fijo());
            
            int resultado=stmt.executeUpdate();
            stmt.close();
            return resultado;
        }catch(Exception e){
            throw e;
        }
    }
    
    
    
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @return
     * @throws Exception 
     */
    
    
    public List<ActivosFijosDTO> select() throws Exception{
        try{
            List<ActivosFijosDTO> l_resultado = new ArrayList<>();
            String sql = "SELECT activos_fijos.id_activo_fijo, "; // 1
            sql =sql + "activos_fijos.nombre,"; // 2
            sql = sql + "activos_fijos.descripcion,"; // 3
            sql = sql + "activos_fijos.tipo,";  //4
            sql = sql + "activos_fijos.serial, ";  //5
            sql = sql + "activos_fijos.numeroInternoInventario,";  //6
            sql = sql + "activos_fijos.peso,";  //7
            sql = sql + "activos_fijos.ancho,";  //8
            sql = sql + "activos_fijos.alto,";  //9
            sql = sql + "activos_fijos.largo, ";  //10
            sql = sql + "activos_fijos.valorCompra,";  //11
            sql = sql + "activos_fijos.fechaCompra,";  //12
            sql = sql + "activos_fijos.fechaDeBaja,";  //13
            sql = sql + "estado_actual.estado_actual,";  //14
            sql = sql + "activos_fijos.color ";  //15
            sql = sql + "FROM estado_actual INNER JOIN activos_fijos ON estado_actual.id_estado_actual = activos_fijos.estadoActual ";
            
            Statement stmt = this.conexion.createStatement();
            ResultSet rs =  stmt.executeQuery(sql);
            while(rs.next()){
                ActivosFijosDTO activosfijosDTO = new ActivosFijosDTO();
                activosfijosDTO.setId_activo_fijo(rs.getString(1));
                activosfijosDTO.setNombre(rs.getString(2));
                activosfijosDTO.setDescripcion(rs.getString(3));
                activosfijosDTO.setTipo(rs.getString(4));
                activosfijosDTO.setSerial(rs.getString(5));
                activosfijosDTO.setNumeroInternoInventario(rs.getString(6));
                activosfijosDTO.setPeso(rs.getString(7));
                activosfijosDTO.setAncho(rs.getString(8));
                activosfijosDTO.setAlto(rs.getString(9));
                activosfijosDTO.setLargo(rs.getString(10));
                activosfijosDTO.setValorCompra(rs.getString(11));
                activosfijosDTO.setFechaCompra(rs.getString(12));
                activosfijosDTO.setFechaDeBaja(rs.getString(13));
                activosfijosDTO.setEstadoActual(rs.getString(14));
                activosfijosDTO.setColor(rs.getString(15));
                
                l_resultado.add(activosfijosDTO);
            }
            
            stmt.close();
            return l_resultado;
        }catch(Exception e){
            throw e;
        }
    }
            
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @return
     * @throws Exception 
     */
    
    
    public List<ActivosFijosDTO> select(ConsultaActivosFijosDTO consultaDTO) throws Exception{
        try{
            Utilitario util = new Utilitario();
            List<ActivosFijosDTO> l_resultado = new ArrayList<>();
            String sql = "SELECT activos_fijos.id_activo_fijo, "; // 1
            sql =sql + "activos_fijos.nombre,"; // 2
            sql = sql + "activos_fijos.descripcion,"; // 3
            sql = sql + "activos_fijos.tipo,";  //4
            sql = sql + "activos_fijos.serial, ";  //5
            sql = sql + "activos_fijos.numeroInternoInventario,";  //6
            sql = sql + "activos_fijos.peso,";  //7
            sql = sql + "activos_fijos.ancho,";  //8
            sql = sql + "activos_fijos.alto,";  //9
            sql = sql + "activos_fijos.largo, ";  //10
            sql = sql + "activos_fijos.valorCompra,";  //11
            sql = sql + "activos_fijos.fechaCompra,";  //12
            sql = sql + "activos_fijos.fechaDeBaja,";  //13
            sql = sql + "estado_actual.estado_actual,";  //14
            sql = sql + "activos_fijos.color ";  //15
            sql = sql + "FROM estado_actual INNER JOIN activos_fijos ON estado_actual.id_estado_actual = activos_fijos.estadoActual ";
            
            if (util.esIgualANuloLaCadena(consultaDTO.getTipo())==false){
                sql = sql + "WHERE tipo =  '"+consultaDTO.getTipo()+"'  ";
            }else if (util.esIgualANuloLaCadena(consultaDTO.getFechaCompra())==false){
                sql = sql + "WHERE fechaCompra =  '"+consultaDTO.getFechaCompra()+"'  ";
            }else if (util.esIgualANuloLaCadena(consultaDTO.getSerial())==false){
                sql = sql + "WHERE serial =  '"+consultaDTO.getSerial()+"'  ";
            }
            
            Statement stmt = this.conexion.createStatement();
            ResultSet rs =  stmt.executeQuery(sql);
            while(rs.next()){
                ActivosFijosDTO activosfijosDTO = new ActivosFijosDTO();
                activosfijosDTO.setId_activo_fijo(rs.getString(1));
                activosfijosDTO.setNombre(rs.getString(2));
                activosfijosDTO.setDescripcion(rs.getString(3));
                activosfijosDTO.setTipo(rs.getString(4));
                activosfijosDTO.setSerial(rs.getString(5));
                activosfijosDTO.setNumeroInternoInventario(rs.getString(6));
                activosfijosDTO.setPeso(rs.getString(7));
                activosfijosDTO.setAncho(rs.getString(8));
                activosfijosDTO.setAlto(rs.getString(9));
                activosfijosDTO.setLargo(rs.getString(10));
                activosfijosDTO.setValorCompra(rs.getString(11));
                activosfijosDTO.setFechaCompra(rs.getString(12));
                activosfijosDTO.setFechaDeBaja(rs.getString(13));
                activosfijosDTO.setEstadoActual(rs.getString(14));
                activosfijosDTO.setColor(rs.getString(15));
                
                l_resultado.add(activosfijosDTO);
            }
            
            stmt.close();
            return l_resultado;
        }catch(Exception e){
            throw e;
        }
    }
            
    
}
