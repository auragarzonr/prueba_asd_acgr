/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.dto.resultados;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User
 */
@XmlRootElement
public class Resultado_crearNuevoActivoFijoDTO {
    
    @XmlElement private ResultadoGenericoDTO resultadoGenericoDTO;

    public Resultado_crearNuevoActivoFijoDTO() {
        resultadoGenericoDTO = new ResultadoGenericoDTO();
    }

    public ResultadoGenericoDTO getResultadoGenericoDTO() {
        return resultadoGenericoDTO;
    }

    public void setResultadoGenericoDTO(ResultadoGenericoDTO resultadoGenericoDTO) {
        this.resultadoGenericoDTO = resultadoGenericoDTO;
    }
    
    
    
}
