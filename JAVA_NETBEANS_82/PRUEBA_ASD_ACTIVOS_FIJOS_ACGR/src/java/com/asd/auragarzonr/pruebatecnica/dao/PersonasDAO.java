/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.dao;

import com.asd.auragarzonr.pruebatecnica.dto.AreasDTO;
import com.asd.auragarzonr.pruebatecnica.dto.PersonasDTO;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 5 de Marzo de 2019 12:37 pm
 */
public class PersonasDAO {

    private final Connection conexion;
    final static Logger logger = Logger.getLogger(AreasDAO.class);

    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de Marzo de 2019 12:38 pm
     * @param conexion
     */
    public PersonasDAO(Connection conexion) {
        this.conexion = conexion;
    }
/**
 * @author Aura Cristina Garzon Rodriguez
 * @since 5 de marzo de 2019
 * @param personasDAO 
 */
    public void insert(PersonasDAO personasDAO) {
        try {
            String sql = "INSERT INTO activos_fijos_acgr.personas\n"
                    + "(id_persona,\n"
                    + "nombre_persona)\n"
                    + "VALUES\n"
                    + "(<{id_persona: }>,\n"
                    + "<{nombre_persona: }>);";

        } catch (Exception e) {
            throw e;
        }
    }
    
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019 2:50 pm
     * @return
     * @throws Exception 
     */

    public List<PersonasDTO> select() throws Exception {
        try {
            
            List<PersonasDTO> l_resultado = new ArrayList<>();
            String sql = "SELECT personas.id_persona, personas.nombre_persona FROM activos_fijos_acgr.personas";

            Statement stmt = this.conexion.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                PersonasDTO personasDTO = new PersonasDTO();
                personasDTO.setId_persona(rs.getString(1));
                personasDTO.setNombrePersona(rs.getString(2));
                l_resultado.add(personasDTO);
            }

            stmt.close();
            return l_resultado;

        } catch (Exception e) {
            throw e;
        }
    }

}
