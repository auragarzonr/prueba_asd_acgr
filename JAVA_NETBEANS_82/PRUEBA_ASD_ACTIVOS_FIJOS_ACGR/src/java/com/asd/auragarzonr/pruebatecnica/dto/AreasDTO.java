/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since  5 de Marzo de 2019 11:17 AM
 */
@XmlRootElement
public class AreasDTO {
    @XmlElement private String id_area;
    @XmlElement private String area;

    public String getId_area() {
        return id_area;
    }

    public void setId_area(String id_area) {
        this.id_area = id_area;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
/**
 * @Author Aura Cristina Garzon Rodriguez
 * @since 5 de marzo de 2019
 * @param string 
 */
    public void seti(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
