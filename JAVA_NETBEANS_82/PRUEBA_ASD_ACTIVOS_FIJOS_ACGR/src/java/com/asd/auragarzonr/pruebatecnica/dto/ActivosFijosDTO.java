/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 5 Marzo 2019 10:48 AM
 */
@XmlRootElement
public class ActivosFijosDTO {
    
    @XmlElement private String id_activo_fijo;
    @XmlElement private String nombre;
    @XmlElement private String descripcion;
    @XmlElement private String tipo;
    @XmlElement private String serial;
    @XmlElement private String numeroInternoInventario;
    @XmlElement private String peso;
    @XmlElement private String ancho;
    @XmlElement private String alto;
    @XmlElement private String largo;         
    @XmlElement private String valorCompra;
    @XmlElement private String fechaCompra;
    @XmlElement private String fechaDeBaja;
    @XmlElement private String estadoActual;
    @XmlElement private String color;

    public String getId_activo_fijo() {
        return id_activo_fijo;
    }

    public void setId_activo_fijo(String id_activo_fijo) {
        this.id_activo_fijo = id_activo_fijo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getNumeroInternoInventario() {
        return numeroInternoInventario;
    }

    public void setNumeroInternoInventario(String numeroInternoInventario) {
        this.numeroInternoInventario = numeroInternoInventario;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getAncho() {
        return ancho;
    }

    public void setAncho(String ancho) {
        this.ancho = ancho;
    }

    public String getAlto() {
        return alto;
    }

    public void setAlto(String alto) {
        this.alto = alto;
    }

    public String getLargo() {
        return largo;
    }

    public void setLargo(String largo) {
        this.largo = largo;
    }

    public String getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(String valorCompra) {
        this.valorCompra = valorCompra;
    }

    public String getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(String fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public String getFechaDeBaja() {
        return fechaDeBaja;
    }

    public void setFechaDeBaja(String fechaDeBaja) {
        this.fechaDeBaja = fechaDeBaja;
    }

    public String getEstadoActual() {
        return estadoActual;
    }

    public void setEstadoActual(String estadoActual) {
        this.estadoActual = estadoActual;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    
}
