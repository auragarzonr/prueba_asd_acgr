/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.facade;

import com.asd.auragarzonr.pruebatecnica.dao.AreasDAO;
import com.asd.auragarzonr.pruebatecnica.dto.AreasDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_consultarAreasDTO;
import com.asd.auragarzonr.pruebatecnica.persistence.DataSourceFactory;
import java.util.List;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since  5 de marzo de 2019 3:00 pm
 */
public class AreasFacade {
    
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @return
     * @throws Exception 
     */   
    
    public Resultado_consultarAreasDTO consultarAreas() throws Exception{
        DataSourceFactory ds = new DataSourceFactory();
        Resultado_consultarAreasDTO resultadoDTO = new Resultado_consultarAreasDTO();
        AreasDAO areasDAO = new AreasDAO(ds.obtenerConexion());
        List<AreasDTO> l_resultado = areasDAO.select();
        if (l_resultado.size()>1){
            resultadoDTO.getResultadoGenericoDTO().setCodigoEjecucion(200);
            resultadoDTO.getResultadoGenericoDTO().setDescripcionEjecucion("La consulta ha traido resultados");
        }else{
            resultadoDTO.getResultadoGenericoDTO().setCodigoEjecucion(400);
            resultadoDTO.getResultadoGenericoDTO().setDescripcionEjecucion("La consulta no ha traido resultados");            
        }
        resultadoDTO.setL_resultado(l_resultado);
        return resultadoDTO;
    }
    
}
