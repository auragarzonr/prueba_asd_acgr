/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 5 de Marzo de 2019 11:23 AM
 */
@XmlRootElement
public class AsignacionesActivosFijosPersonasDTO {
    @XmlElement private int id_activoFijo;
    @XmlElement private int id_persona;
    @XmlElement private String fechaAsignacion;    

    public int getId_activoFijo() {
        return id_activoFijo;
    }

    public void setId_activoFijo(int id_activoFijo) {
        this.id_activoFijo = id_activoFijo;
    }

    public int getId_persona() {
        return id_persona;
    }

    public void setId_persona(int id_persona) {
        this.id_persona = id_persona;
    }

    public String getFechaAsignacion() {
        return fechaAsignacion;
    }

    public void setFechaAsignacion(String fechaAsignacion) {
        this.fechaAsignacion = fechaAsignacion;
    }
    
    
}
