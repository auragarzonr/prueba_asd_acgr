/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.dto.resultados;

import com.asd.auragarzonr.pruebatecnica.dto.ActivosFijosDTO;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author User
 */
public class Resultado_consultarActivosFijos2DTO {
    
    @XmlElement private ResultadoGenericoDTO resultadoGenericoDTO;
    @XmlElement private List<ActivosFijosDTO> l_resultado;

    public Resultado_consultarActivosFijos2DTO() {
        resultadoGenericoDTO = new ResultadoGenericoDTO();        
    }

    public ResultadoGenericoDTO getResultadoGenericoDTO() {
        return resultadoGenericoDTO;
    }

    public void setResultadoGenericoDTO(ResultadoGenericoDTO resultadoGenericoDTO) {
        this.resultadoGenericoDTO = resultadoGenericoDTO;
    }

    public List<ActivosFijosDTO> getL_resultado() {
        return l_resultado;
    }

    public void setL_resultado(List<ActivosFijosDTO> l_resultado) {
        this.l_resultado = l_resultado;
    }
    
    
    
    
    
}
