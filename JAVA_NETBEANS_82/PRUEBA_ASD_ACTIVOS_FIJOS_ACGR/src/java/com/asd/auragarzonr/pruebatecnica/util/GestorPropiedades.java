/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 6 de marzo de 2019
 */
public class GestorPropiedades {

    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 6 de marzo de 2019
     * @param propiedad
     * @return 
     */
    
    public String obtenerValorPropiedad(String propiedad) {
        InputStream input = null;
        String valorPropiedad = null;
        try {
            Properties prop = new Properties();
            input = new FileInputStream("c:\\Temp\\config.properties");
            prop.load(input);
            valorPropiedad = prop.getProperty(propiedad);
            return valorPropiedad;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GestorPropiedades.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GestorPropiedades.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return valorPropiedad;
    }

}
