/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.facade;

import com.asd.auragarzonr.pruebatecnica.dao.ActivosFijosDAO;
import com.asd.auragarzonr.pruebatecnica.dto.ActivosFijosDTO;
import com.asd.auragarzonr.pruebatecnica.dto.consulta.ConsultaActivosFijosDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_actualizarNuevoActivoFijoDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_consultarActivosFijos2DTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_consultarActivosFijosDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_crearNuevoActivoFijoDTO;
import com.asd.auragarzonr.pruebatecnica.persistence.DataSourceFactory;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 5 de marzo de 2019
 */
public class ActivosFijosFacade {
    
    
    /**
     * @authro Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @param activoDTO
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception 
     */
    
    public Resultado_crearNuevoActivoFijoDTO crearNuevoActivoFijo(ActivosFijosDTO activoDTO) throws ClassNotFoundException, SQLException, Exception{
        DataSourceFactory ds = new DataSourceFactory();
        Resultado_crearNuevoActivoFijoDTO resultadoDTO = new Resultado_crearNuevoActivoFijoDTO();
        ActivosFijosDAO activosfijosDAO = new ActivosFijosDAO(ds.obtenerConexion());
        int resultado=activosfijosDAO.insert(activoDTO);
        ds.cerrarConexion();     
        if (resultado==1){
            resultadoDTO.getResultadoGenericoDTO().setCodigoEjecucion(200);
            resultadoDTO.getResultadoGenericoDTO().setDescripcionEjecucion("Insercion llevada a cabo con exito");
        }else{
            resultadoDTO.getResultadoGenericoDTO().setCodigoEjecucion(400);
            resultadoDTO.getResultadoGenericoDTO().setDescripcionEjecucion("Hubo datos faltantes al crear o actualizar un activo");            
        }
        return resultadoDTO;
    }
    
    /**
     * @autrhor Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @param activoDTO
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception 
     */
    
    public Resultado_actualizarNuevoActivoFijoDTO actualizarNuevoActivoFijo(ActivosFijosDTO activoDTO) throws ClassNotFoundException, SQLException, Exception{
        DataSourceFactory ds = new DataSourceFactory();
        Resultado_actualizarNuevoActivoFijoDTO resultadoDTO = new Resultado_actualizarNuevoActivoFijoDTO();
        ActivosFijosDAO activosfijosDAO = new ActivosFijosDAO(ds.obtenerConexion());
        int resultado=activosfijosDAO.update(activoDTO);
        ds.cerrarConexion();        
        if (resultado==1){
            resultadoDTO.getResultadoGenericoDTO().setCodigoEjecucion(200);
            resultadoDTO.getResultadoGenericoDTO().setDescripcionEjecucion("Actualizacion llevada a cabo con exito");
        }else{
            resultadoDTO.getResultadoGenericoDTO().setCodigoEjecucion(400);
            resultadoDTO.getResultadoGenericoDTO().setDescripcionEjecucion("Hubo datos faltantes al crear o actualizar un activo");            
        }        
        return resultadoDTO;
    }
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @return
     * @throws Exception 
     */
    
    public Resultado_consultarActivosFijosDTO consultarActivosFijos() throws Exception{
        DataSourceFactory ds = new DataSourceFactory();
        Resultado_consultarActivosFijosDTO resultadoDTO = new Resultado_consultarActivosFijosDTO();
        ActivosFijosDAO activosfijosDAO = new ActivosFijosDAO(ds.obtenerConexion());
        List<ActivosFijosDTO> l_resultado = activosfijosDAO.select();
        if (l_resultado.size()>1){
            resultadoDTO.getResultadoGenericoDTO().setCodigoEjecucion(200);
            resultadoDTO.getResultadoGenericoDTO().setDescripcionEjecucion("La consulta ha traido resultados");
        }else{
            resultadoDTO.getResultadoGenericoDTO().setCodigoEjecucion(400);
            resultadoDTO.getResultadoGenericoDTO().setDescripcionEjecucion("La consulta no ha traido resultados");            
        }
        resultadoDTO.setL_resultado(l_resultado);
        ds.cerrarConexion();
        return resultadoDTO;
    }
    
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @return
     * @throws Exception 
     */
    
    public Resultado_consultarActivosFijos2DTO consultarActivosFijos(ConsultaActivosFijosDTO consultaDTO) throws Exception{
        DataSourceFactory ds = new DataSourceFactory();
        Resultado_consultarActivosFijos2DTO resultadoDTO = new Resultado_consultarActivosFijos2DTO();
        ActivosFijosDAO activosfijosDAO = new ActivosFijosDAO(ds.obtenerConexion());
        List<ActivosFijosDTO> l_resultado = activosfijosDAO.select(consultaDTO);
        if (l_resultado.size()>1){
            resultadoDTO.getResultadoGenericoDTO().setCodigoEjecucion(200);
            resultadoDTO.getResultadoGenericoDTO().setDescripcionEjecucion("La consulta ha traido resultados");
        }else{
            resultadoDTO.getResultadoGenericoDTO().setCodigoEjecucion(400);
            resultadoDTO.getResultadoGenericoDTO().setDescripcionEjecucion("La consulta no ha traido resultados");            
        }
        resultadoDTO.setL_resultado(l_resultado);
        ds.cerrarConexion();
        return resultadoDTO;
    }    
}
