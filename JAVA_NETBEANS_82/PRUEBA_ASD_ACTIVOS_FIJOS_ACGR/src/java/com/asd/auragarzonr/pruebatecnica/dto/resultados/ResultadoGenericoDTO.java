/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.dto.resultados;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User
 */
@XmlRootElement
public class ResultadoGenericoDTO {
    @XmlElement private int codigoEjecucion;
    @XmlElement private String descripcionEjecucion;

    public ResultadoGenericoDTO() {
    }
    
    

    public int getCodigoEjecucion() {
        return codigoEjecucion;
    }

    public void setCodigoEjecucion(int codigoEjecucion) {
        this.codigoEjecucion = codigoEjecucion;
    }

    public String getDescripcionEjecucion() {
        return descripcionEjecucion;
    }

    public void setDescripcionEjecucion(String descripcionEjecucion) {
        this.descripcionEjecucion = descripcionEjecucion;
    }
    
    
    
}
