/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.persistence;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.asd.auragarzonr.pruebatecnica.util.GestorPropiedades;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 5 de marzo de 2019 3:10 pm
 */
public class DataSourceFactory {

    public static Connection conexion;

    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 Marzo 2019 3:18 pm
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public DataSourceFactory() throws ClassNotFoundException, SQLException {
        GestorPropiedades gestor = new GestorPropiedades();
        String JDBC_DRIVER = gestor.obtenerValorPropiedad("JDBC_DRIVER");
        String DB_URL = "jdbc:mysql://"+gestor.obtenerValorPropiedad("SERVIDOR")+":"+gestor.obtenerValorPropiedad("PUERTO")+"/"+gestor.obtenerValorPropiedad("BASE_DATOS");
        String USER = gestor.obtenerValorPropiedad("USUARIO_BASE_DATOS");
        String PASS = gestor.obtenerValorPropiedad("CONTRASENA_BASE_DATOS");
        Class.forName(JDBC_DRIVER);
        conexion = DriverManager.getConnection(DB_URL, USER, PASS);
    }

    /**
     *@author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019 3:20 pm
     * @return
     */
    public Connection obtenerConexion() throws SQLException {
        return conexion;
    }

    /**
     *@author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019 3:21 pm
     * @throws SQLException
     */
    public void cerrarConexion() throws SQLException {
        conexion.close();
    }
}
