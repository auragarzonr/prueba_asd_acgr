/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.rest;

import com.asd.auragarzonr.pruebatecnica.dto.ActivosFijosDTO;
import com.asd.auragarzonr.pruebatecnica.dto.AreasDTO;
import com.asd.auragarzonr.pruebatecnica.dto.PersonasDTO;
import com.asd.auragarzonr.pruebatecnica.dto.consulta.ConsultaActivosFijosDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_actualizarNuevoActivoFijoDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_consultarActivosFijos2DTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_consultarActivosFijosDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_consultarAreasDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_consultarPersonasDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_crearNuevoActivoFijoDTO;
import com.asd.auragarzonr.pruebatecnica.facade.ActivosFijosFacade;
import com.asd.auragarzonr.pruebatecnica.facade.AreasFacade;
import com.asd.auragarzonr.pruebatecnica.facade.PersonasFacade;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 5 de marzo de 2019 3:23pm
 */
@Path("ActivosFijosServicioREST")
public class ActivosFijosServicioREST {
    
    @Context
    private UriInfo context;
    
    
    
    
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @return
     * @throws Exception 
     */
    @Path("consultarAreas")
    @POST
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Resultado_consultarAreasDTO consultarAreas() throws Exception{
        AreasFacade areasfacade = new AreasFacade();
        return areasfacade.consultarAreas();
    }
    
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @return
     * @throws Exception 
     */
    
    @Path("consutlarPersonas")
    @POST
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Resultado_consultarPersonasDTO consultarPersonas() throws Exception{
        PersonasFacade personasfacade = new PersonasFacade();
        return personasfacade.consultarPersonas();
    }
    
    /**
     * @authro Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @return
     * @throws Exception 
     */
    
    @Path("consultarActivosFijos")
    @POST
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Resultado_consultarActivosFijosDTO consultarActivosFijos() throws Exception{
        ActivosFijosFacade activosfijosfacade = new ActivosFijosFacade();
        return activosfijosfacade.consultarActivosFijos();
    }
    
    
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @return
     * @throws Exception 
     */
    
    @Path("consultarActivosFijosParametros")
    @POST
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)    
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Resultado_consultarActivosFijos2DTO consultarActivosFijos(ConsultaActivosFijosDTO consultaDTO) throws Exception{
        ActivosFijosFacade activosfijosfacade = new ActivosFijosFacade();
        return activosfijosfacade.consultarActivosFijos(consultaDTO);
    }    
    
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @param activosDTO
     * @throws Exception 
     */
    
    @Path("crearNuevoActivoFijo")
    @POST
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)    
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Resultado_crearNuevoActivoFijoDTO crearNuevoActivoFijo(ActivosFijosDTO activosDTO) throws Exception{
        Resultado_crearNuevoActivoFijoDTO resultadoDTO = new Resultado_crearNuevoActivoFijoDTO();
        ActivosFijosFacade activosfijosfacade = new ActivosFijosFacade();
        resultadoDTO=activosfijosfacade.crearNuevoActivoFijo(activosDTO);
        return resultadoDTO;
    }  
    
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @param activosDTO
     * @throws Exception 
     */
    
    @Path("actualizarNuevoActivoFijo")
    @POST
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)    
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Resultado_actualizarNuevoActivoFijoDTO actualizarNuevoActivoFijo(ActivosFijosDTO activosDTO) throws Exception{
        ActivosFijosFacade activosfijosfacade = new ActivosFijosFacade();
        return activosfijosfacade.actualizarNuevoActivoFijo(activosDTO);
    }       
    
}
