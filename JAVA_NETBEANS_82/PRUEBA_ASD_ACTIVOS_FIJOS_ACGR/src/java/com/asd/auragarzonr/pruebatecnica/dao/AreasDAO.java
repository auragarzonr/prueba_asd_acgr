/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.dao;

import com.asd.auragarzonr.pruebatecnica.dto.AreasDTO;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 5 de Marzo de 2019 12:11 AM
 */
public class AreasDAO {

    private final Connection conexion;
    final static Logger logger = Logger.getLogger(AreasDAO.class);

    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de Marzo de 2019 12:12 AM
     * @param conexion
     */
    public AreasDAO(Connection conexion) {
        this.conexion = conexion;
    }

    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de Marzo de 2019 12:13 AM
     * @param areasDAO
     */
    public void insert(AreasDAO areasDAO) {
        try {
            String sql = "INSERT INTO activos_fijos_acgr.areas\n"
                    + "(id_area,\n"
                    + "area)\n"
                    + "VALUES\n"
                    + "(<{id_area: }>,\n"
                    + "<{area: }>);";
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * @author Aura Cristina Garzón Rodriguez
     * @since 5 de Marzo de 2019 2:40 pm
     * @return
     * @throws Exception
     */
    public List<AreasDTO> select() throws Exception {
        try {

            logger.info("Entra a método consulta de areas");
            List<AreasDTO> l_resultado = new ArrayList<>();
            String sql = "SELECT areas.id_area, areas.area FROM activos_fijos_acgr.areas";

            Statement stmt = this.conexion.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                AreasDTO areaDTO = new AreasDTO();
                areaDTO.setId_area(rs.getString(1));
                areaDTO.setArea(rs.getString(2));
                l_resultado.add(areaDTO);
            }

            stmt.close();
            return l_resultado;

        } catch (Exception e) {
            logger.info("Ocurre un error en el metodo de consulta de areas");
            throw e;
        }
    }

}
