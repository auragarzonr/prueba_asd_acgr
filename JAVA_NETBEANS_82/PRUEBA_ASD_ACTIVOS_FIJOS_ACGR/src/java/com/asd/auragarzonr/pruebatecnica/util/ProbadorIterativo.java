/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 6 de marzo de 2019
 */
public class ProbadorIterativo {
    
    public static void main(String args[]){
      GestorPropiedades gestor = new GestorPropiedades();
        System.out.println(gestor.obtenerValorPropiedad("CAMPO1"));
        
      ResourceBundle bundle = ResourceBundle.getBundle("Messages", Locale.US);  
      System.out.println("Message in "+Locale.US +": "+bundle.getString("mensaje"));  

      bundle = ResourceBundle.getBundle("Messages", Locale.FRANCE);  
      System.out.println("Message in "+Locale.FRANCE +": "+bundle.getString("mensaje"));        
      
      Locale spanishLocale = new Locale("es","ES");
      bundle = ResourceBundle.getBundle("Messages", spanishLocale);  
      //System.out.println("Message in "+Locale +": "+bundle.getString("mensaje"));        
    }
    
}
