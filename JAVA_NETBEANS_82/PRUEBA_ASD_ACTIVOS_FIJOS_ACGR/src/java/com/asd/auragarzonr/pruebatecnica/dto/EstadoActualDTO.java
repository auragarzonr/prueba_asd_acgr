/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 5 de Marzo de 2019 11:28 AM
 */
@XmlRootElement
public class EstadoActualDTO {
    @XmlElement private int id_estadoActual;
    @XmlElement private String estadoActual;    

    public int getId_estadoActual() {
        return id_estadoActual;
    }

    public void setId_estadoActual(int id_estadoActual) {
        this.id_estadoActual = id_estadoActual;
    }

    public String getEstadoActual() {
        return estadoActual;
    }

    public void setEstadoActual(String estadoActual) {
        this.estadoActual = estadoActual;
    }
    
    
}
