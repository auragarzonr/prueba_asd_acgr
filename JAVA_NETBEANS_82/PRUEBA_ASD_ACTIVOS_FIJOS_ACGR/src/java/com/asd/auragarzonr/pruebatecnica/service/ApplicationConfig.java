/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 5 de marzo de 2019
 */
@javax.ws.rs.ApplicationPath("ActivosFijosRutaPrincipal")
public class ApplicationConfig extends Application {
/**
 * @author Aura Cristina Garzon Rodriguez
 * @since 5 de marzo de 2019
 * @return 
 */
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * @author Aura Cristina Garzon Rodriguez 
     * @since 5 de marzo de 2019
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
    }
    
}
