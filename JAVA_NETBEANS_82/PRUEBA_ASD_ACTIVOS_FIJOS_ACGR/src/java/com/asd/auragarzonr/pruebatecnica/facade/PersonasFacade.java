/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.facade;

import com.asd.auragarzonr.pruebatecnica.dao.PersonasDAO;
import com.asd.auragarzonr.pruebatecnica.dto.PersonasDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_consultarPersonasDTO;
import com.asd.auragarzonr.pruebatecnica.persistence.DataSourceFactory;
import java.util.List;

/**
 *
 * @author Aura Garzon Rodriguez
 * @since 5 de marzo de 2019 3:10 pm
 */
public class PersonasFacade {
    
    /**
     * @author Aura Cristina Garzon Rodriguez
     * @since 5 de marzo de 2019
     * @return
     * @throws Exception 
     */
    
    public Resultado_consultarPersonasDTO consultarPersonas() throws Exception{
        DataSourceFactory ds = new DataSourceFactory();
        Resultado_consultarPersonasDTO resultadoDTO = new Resultado_consultarPersonasDTO();
        PersonasDAO personasDAO = new PersonasDAO(ds.obtenerConexion());
        List<PersonasDTO> l_resultado = personasDAO.select();
        if (l_resultado.size()>1){
            resultadoDTO.getResultadoGenericoDTO().setCodigoEjecucion(200);
            resultadoDTO.getResultadoGenericoDTO().setDescripcionEjecucion("La consulta ha traido resultados");
        }else{
            resultadoDTO.getResultadoGenericoDTO().setCodigoEjecucion(400);
            resultadoDTO.getResultadoGenericoDTO().setDescripcionEjecucion("La consulta no ha traido resultados");            
        }        
        resultadoDTO.setL_resultado(l_resultado);
        return resultadoDTO;
    }
    
}
