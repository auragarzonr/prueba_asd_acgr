/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.util;

/**
 *
 * @author Aura Cristina Garzon Rodriguez
 * @since 6 de marzo de 2019
 */
public class Utilitario {
    
    public boolean esIgualANuloLaCadena(String cadena) {
        if (cadena == null) {
            return true;
        } else if (cadena != null && cadena.equals("null")) {
            return true;
        } else if (cadena != null && cadena.trim().equals("")) {
            return true;
        } else {
            return false;
        }
    }    
    
}
