/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asd.auragarzonr.pruebatecnica.rest;

import com.asd.auragarzonr.pruebatecnica.dto.ActivosFijosDTO;
import com.asd.auragarzonr.pruebatecnica.dto.consulta.ConsultaActivosFijosDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_actualizarNuevoActivoFijoDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_consultarActivosFijos2DTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_consultarActivosFijosDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_consultarAreasDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_consultarPersonasDTO;
import com.asd.auragarzonr.pruebatecnica.dto.resultados.Resultado_crearNuevoActivoFijoDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author User
 */
public class ActivosFijosServicioRESTIT {
    
    public ActivosFijosServicioRESTIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of consultarAreas method, of class ActivosFijosServicioREST.
     */
    @Test
    public void testConsultarAreas() throws Exception {
        System.out.println("consultarAreas");
        ActivosFijosServicioREST instance = new ActivosFijosServicioREST();
        Resultado_consultarAreasDTO expResult = null;
        Resultado_consultarAreasDTO result = instance.consultarAreas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of consultarPersonas method, of class ActivosFijosServicioREST.
     */
    @Test
    public void testConsultarPersonas() throws Exception {
        System.out.println("consultarPersonas");
        ActivosFijosServicioREST instance = new ActivosFijosServicioREST();
        Resultado_consultarPersonasDTO expResult = null;
        Resultado_consultarPersonasDTO result = instance.consultarPersonas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of consultarActivosFijos method, of class ActivosFijosServicioREST.
     */
    @Test
    public void testConsultarActivosFijos_0args() throws Exception {
        System.out.println("consultarActivosFijos");
        ActivosFijosServicioREST instance = new ActivosFijosServicioREST();
        Resultado_consultarActivosFijosDTO expResult = null;
        Resultado_consultarActivosFijosDTO result = instance.consultarActivosFijos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of consultarActivosFijos method, of class ActivosFijosServicioREST.
     */
    @Test
    public void testConsultarActivosFijos_ConsultaActivosFijosDTO() throws Exception {
        System.out.println("consultarActivosFijos");
        ConsultaActivosFijosDTO consultaDTO = null;
        ActivosFijosServicioREST instance = new ActivosFijosServicioREST();
        Resultado_consultarActivosFijos2DTO expResult = null;
        Resultado_consultarActivosFijos2DTO result = instance.consultarActivosFijos(consultaDTO);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of crearNuevoActivoFijo method, of class ActivosFijosServicioREST.
     */
    @Test
    public void testCrearNuevoActivoFijo() throws Exception {
        System.out.println("crearNuevoActivoFijo");
        ActivosFijosDTO activosDTO = new ActivosFijosDTO();
        activosDTO.setNombre("nombre1");
        activosDTO.setDescripcion("descripcion1");
        activosDTO.setTipo("tipo 1");
        activosDTO.setSerial("serial");
        activosDTO.setNumeroInternoInventario("NoInternoInventario 1");
        activosDTO.setPeso("perso 1");
        activosDTO.setAncho("ancho 1");
        activosDTO.setAlto("alto 1");
        activosDTO.setLargo("largo 1");
        activosDTO.setValorCompra("100");
        activosDTO.setFechaCompra("2019-03-05");
        activosDTO.setFechaDeBaja("2019-03-05");
        activosDTO.setEstadoActual("1");
        activosDTO.setColor("color 1");
        ActivosFijosServicioREST instance = new ActivosFijosServicioREST();
        Resultado_crearNuevoActivoFijoDTO expResult = null;
        Resultado_crearNuevoActivoFijoDTO result = instance.crearNuevoActivoFijo(activosDTO);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of actualizarNuevoActivoFijo method, of class ActivosFijosServicioREST.
     */
    @Test
    public void testActualizarNuevoActivoFijo() throws Exception {
        System.out.println("actualizarNuevoActivoFijo");
        ActivosFijosDTO activosDTO = null;
        ActivosFijosServicioREST instance = new ActivosFijosServicioREST();
        Resultado_actualizarNuevoActivoFijoDTO expResult = null;
        Resultado_actualizarNuevoActivoFijoDTO result = instance.actualizarNuevoActivoFijo(activosDTO);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
